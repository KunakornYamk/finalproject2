﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnTriggirWin : MonoBehaviour
{
    [SerializeField] private string strTag;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(strTag))
        {
            SceneManager.LoadScene(5);
        }
        
    }
}
