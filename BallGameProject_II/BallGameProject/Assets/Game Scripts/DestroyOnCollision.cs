﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DestroyOnCollision : MonoBehaviour
{
    [SerializeField] private string strTag;
    [SerializeField] private UnityEvent Istouch;

    private bool touch = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(strTag) && touch == false)
        {
            touch = true;
          
            this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            
            Istouch.Invoke();
            touch = false;
        }
    }
}
