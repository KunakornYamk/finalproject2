﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] private Transform transTargets;
   
    // Update is called once per frame
    void Update()
    {
        transform.position = transTargets.position;
    }
}
