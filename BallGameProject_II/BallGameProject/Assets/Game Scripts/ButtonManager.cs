﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{

    public void PlayGameBtnClick()
    {
        SceneManager.LoadScene(2);
    }

    public void HowToPlayBtnClick()
    {
        SceneManager.LoadScene(1);
    }

    public void BackMainmenuBtnClick()
    {
        SceneManager.LoadScene(0);
    }
    
    public void ExitGameBtnClick()
    {
        Application.Quit();
    }

    public void PlayAgainBtnClick()
    {
        SceneManager.LoadScene(2);
    }

    public void Levelone()
    {
        SceneManager.LoadScene(3);
    }
    public void Leveltwo()
    {
        SceneManager.LoadScene(4);
    }
    
}

