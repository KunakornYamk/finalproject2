﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Vector3 v3Force;
    [SerializeField] private KeyCode keyPositive;
    [SerializeField] private KeyCode keyNegative;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(keyPositive))
        GetComponent<Rigidbody>().velocity += v3Force;
        
        if (Input.GetKey(keyNegative))
        GetComponent<Rigidbody>().velocity -= v3Force;
        
    }
}
