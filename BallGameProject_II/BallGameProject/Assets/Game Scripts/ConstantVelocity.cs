﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantVelocity : MonoBehaviour
{
    
    public Vector3 LastPosition;
    public float acceleration;

    void Start()
    {
        LastPosition = transform.position;
    }

    void FixedUpdate()
    {
        acceleration++;
        GetComponent<Rigidbody>().AddRelativeForce((3 * Time.deltaTime) + acceleration ,0 ,0);

        print("Delta position" + (transform.position - LastPosition));
        LastPosition = transform.position;
    }
}
