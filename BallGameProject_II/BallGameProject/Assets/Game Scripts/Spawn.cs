﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour 
{
    [SerializeField] private GameObject object_1;
    [SerializeField] private GameObject object_2;

    private Vector3 object_1_position;
    private Vector3 object_2_position;
    
    [SerializeField] private float WaitTime = 1f;
    private void Start()
    {
        object_1_position = object_1.transform.position;
        object_2_position = object_2.transform.position;
    }

    public void Reset()
    {
        object_1.transform.position = object_1_position;
        object_2.transform.position = object_2_position;

        StartCoroutine(StartMove());
    }

    IEnumerator StartMove()
    {
        yield return new WaitForSeconds(WaitTime);
        object_1.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        object_2.gameObject.GetComponent<Rigidbody>().isKinematic = false;
    }
    
}
 


