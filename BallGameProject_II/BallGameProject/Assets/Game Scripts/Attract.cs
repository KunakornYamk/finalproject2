﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attract : MonoBehaviour
{
    
    
    public Rigidbody mRigidbody;
    
        public float gravity = 6.67f;
    
        void Attraction( Attract objToAttractor )
        {
            Rigidbody mRBToAttractor = objToAttractor.mRigidbody;
    
            Vector3 dir = mRigidbody.position - mRBToAttractor.position;
    
            float distance = dir.magnitude;
    
            float forceMagnitude = gravity*((mRigidbody.mass * mRBToAttractor.mass) / Mathf.Pow(distance, 2));
    
            Vector3 force = dir.normalized * forceMagnitude;
    
            mRBToAttractor.AddForce(force);
           
        }

        
        

        void FixedUpdate()
        {
            Attract[] Attractors = FindObjectsOfType<Attract>();
    
            foreach ( Attract AllPlanet in Attractors )
            {
                if (AllPlanet != this)
                    Attraction(AllPlanet);
            }
        }
}
